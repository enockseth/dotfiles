export TERM="xterm-256color"

source ~/.antigen/antigen.zsh

antigen use oh-my-zsh

antigen bundles <<EOBUNDLES
  rsync
  git
  django
  cp
  python
  colored-man-pages
  command-not-found
  sudo
  supercrabtree/k
  docker
  docker-compose
  bundler
  colorize
  # ubuntu
  archlinux
  virtualenv
  copydir
  zsh-interactive-cd
  timer
  vscode
  # External
  zsh-users/zsh-completions
  zsh-users/zsh-autosuggestions
  esc/conda-zsh-completion
  zsh-users/zsh-syntax-highlighting
  zsh-users/zsh-history-substring-search
  fasd
EOBUNDLES

# Load the theme.
# antigen theme https://github.com/carloscuesta/materialshell  /zsh/materialshell-dark
# antigen theme kennethreitz
antigen theme agnoster

# Apply changes
antigen apply

antigen update

# export BROWSER=firefox
# export BROWSER=chromium
export EDITOR=nvim

# for i3-sensible terminal
export TERMINAL=alacritty

# aliases
if [ -f ~/.zsh_aliases ]; then
  . ~/.zsh_aliases
fi

# Virtualenv 
# export WORKON_HOME=~/.virtualenvs
# . /usr/bin/virtualenvwrapper.sh

# Start xsession if TTY1
# if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
#  exec startx	
# fi

if [[ -z $DISPLAY && $(tty) == /dev/tty1 ]]; then
  XDG_SESSION_TYPE=x11 GDK_BACKEND=x11 exec startx
fi

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi

# source /usr/share/nvm/init-nvm.sh
# /home/enock/.local/share/gem/ruby/3.0.0/bin
export PATH=$PATH:$HOME/.gem/ruby/3.0.0/bin
export PATH=$PATH:$HOME/.bin
# export PATH=~/.config/composer/vendor/bin":$PATH"
export GEM_HOME=$HOME/.gem

# Poweline for ZSH
# powerline-daemon -q
# . /usr/share/powerline/bindings/zsh/powerline.zsh

# GRASS GIS
# export VIRTUALENVWRAPPER_PYTHON=/opt/grass/bin/python

# source ~/.config/broot/launcher/bash/br

# ROS
# source /opt/ros/noetic/setup.zsh

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/home/enock/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#    eval "$__conda_setup"
# else
#    if [ -f "/home/enock/miniconda3/etc/profile.d/conda.sh" ]; then
#        . "/home/enock/miniconda3/etc/profile.d/conda.sh"
#    else
#        export PATH="/home/enock/miniconda3/bin:$PATH"
#    fi
# fi
# unset __conda_setup
# <<< conda initialize <<<

# if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
#        source /etc/profile.d/vte.sh
# fi

# export GDK_SCALE=1

# ruby env
# eval "$(rbenv init -)"

# source /home/enock/.config/broot/launcher/bash/br
# export PATH=~/miniconda/bin:$PATH
# Install Ruby Gems to ~/gems
# export GEM_HOME="$HOME/gems"
# export PATH="$HOME/.gem/bin:$PATH"

export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
# export JAVA_HOME=$(update-java-alternatives -l | tail -n 1 | awk -F ' ' '{print $NF}')


# Powerline
# if [ -f /usr/share/powerline/bindings/zsh/powerline.zsh ]; then
#   powerline-daemon -q
#   POWERLINE_ZSH_CONTINUATION=1
#   POWERLINE_ZSH_SELECT=1
#   source /usr/share/powerline/bindings/zsh/powerline.zsh
# fi

# prompt_context(){}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
eval $(thefuck --alias)

eval "$(oh-my-posh --init --shell zsh --config ~/.poshthemes/craver.omp.json)"
# eval "$(oh-my-posh init zsh)"

# export LD_LIBRARY_PATH=/usr/lib/jvm/java-1.11.0-openjdk-amd64/lib/server:$LD_LIBRARY_PATH
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh

# set PATH so it includes user's private ~/.local/bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# For English terminal responses
export LANG=en_US.UTF-8
export LANGUAGE=en

# source /usr/share/nvm/init-nvm.sh

# MPD
export MPD_HOST=~/.mpd/socket

